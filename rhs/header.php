<?php
/**
 * The header for our theme

 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo( 'template_directory' ); ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo( 'template_directory' ); ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo( 'template_directory' ); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php bloginfo( 'template_directory' ); ?>/site.webmanifest">
<link rel="mask-icon" href="<?php bloginfo( 'template_directory' ); ?>/safari-pinned-tab.svg" color="#00aaad">
<meta name="msapplication-TileColor" content="#2a2e38">
<meta name="theme-color" content="#2a2e38">

<?php wp_head(); ?>

<?php if( get_field('custom_css') ):
    $custom_css = get_field( 'custom_css' ); ?>
    <style type="text/css">
        <?php echo $custom_css;?>
    </style>
<?php endif; ?>

</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'rhs' ); ?></a>

	<header id="masthead" class="site-header" role="banner" style="background: <?php the_field('header_bg_color', 'option') ?> url('<?php the_field('header_bg_image', 'option') ?>')">

    <div class="wrap">
			<div class="navigation-top">

          <div class="header-logo">
                  <a class="" href="<?php echo home_url(); ?>">
                      <img src="<?php the_field('desktop_logo', 'option') ?>" class="logo svg desktop">
                  </a>
          </div>

          <div class="header-menu">
        			<?php
        					wp_nav_menu( array(
        							'theme_location' => 'main',
        							'menu_id'        => 'main-menu',
        							'walker' => new Walker_Nav_Main()
        					) );
        			?>
          </div>

          <div class="header-social">
              <?php if( have_rows('header_social_icons', 'option') ): ?>
                  <div class="social-logos">
                      <?php while( have_rows('header_social_icons', 'option') ): the_row(); ?>
                          <a href="<?php the_sub_field('menu_social_url') ?>" target="_blank">
                              <img class="svg" src="<?php the_sub_field('social_media_icon') ?>">
                          </a>
                      <?php endwhile; ?>
                  </div>
              <?php endif; ?>
          </div>

          <div class="mobile-menu-btn">
              <button data-izimodal-open="#menu-modal" id="menu-btn" class="hamburger hamburger--squeeze" type="button">
                    <span class="hamburger-box">
                      <span class="hamburger-inner"></span>
                    </span>
              </button>
          </div>
        </div>
			</div>

	</header><!-- #masthead -->


		<div id="content" class="site-content">
