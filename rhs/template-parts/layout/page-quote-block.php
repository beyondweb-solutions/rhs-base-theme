<?php
/**
 * Template part for displaying Page Quote layout block
 *
 */


$bg_image = get_sub_field( 'bg_image' );
$cont_width = get_sub_field( 'cont_width' );
$cont_padd = get_sub_field( 'container_padding' );
$cont_align = get_sub_field( 'text_align' );
$row_id = get_sub_field( 'row_id' );
$cont_class = get_sub_field( 'cont_class' );
$bg_colour = get_sub_field( 'bg_color2' );

$pattern_bg = get_sub_field( 'pattern_bg' );
$pattern_type = get_sub_field( 'pattern_type' );
$pattern_one_img = get_sub_field( 'pattern_one_img' );
$pattern_two_img = get_sub_field( 'pattern_two_img' );
$pattern_one_align = get_sub_field( 'pattern_one_align' );
$pattern_two_align = get_sub_field( 'pattern_two_align' );
$enable_curve = get_sub_field( 'enable_curve' );
$curve_pos = get_sub_field( 'curve_pos' );

?>


<section id="<?php echo $row_id ?>" class="layout-block text-block quote-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?> <?php echo $cont_align ?> <?php if ( $enable_curve == 'true' ) { ?><?php echo $curve_pos ?><?php } ?>" style="background: url('<?php echo $bg_image ?>')">

    <div class="wrap <?php echo $cont_width ?>">

        <div class="block-content content-scroll">
          <?php if( have_rows('quote') ): ?>
                <?php while( have_rows('quote') ): the_row();
                $quote_author = get_sub_field( 'quote_author' );
                $quote_text = get_sub_field( 'quote_text' );
                    ?>
                <?php if ($quote_text) { ?>
                      <h2 class="quote"><?php echo $quote_text ?></h2>
                <?php } ?>
                <?php if ($quote_author) { ?>
                      <h5 class="quote-author"><?php echo $quote_author ?></h5>
                <?php } ?>
              <?php endwhile; ?>
          <?php endif; ?>

        </div>

    </div>

    <?php if ( $pattern_bg == 'true' ) { ?>
        <?php if ( $pattern_type == 'one' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
        <?php } elseif ( $pattern_type == 'two' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
          <div class="pattern-bg pattern-two" style="background:url('<?php echo $pattern_two_img ?>') <?php echo $pattern_two_align ?> no-repeat">
          </div>
        <?php } ?>
    <?php } ?>

    <?php if ( $enable_curve == 'true' ) { ?>
          <div class="layout-curve">
            <img class="svg" src="<?php echo get_template_directory_uri(); ?>/assets/images/patterns/rhs-rounded-corner-right.svg" />
          </div>
    <?php } ?>

</section>
