<?php
/**
 * Template part for displaying Contact layout block
 *
 */

 $bg_colour = get_sub_field( 'bg_color' );
 $bg_image = get_sub_field( 'bg_image' );
 $cont_width = get_sub_field( 'cont_width' );
 $cont_padd = get_sub_field( 'container_padding' );
 $cont_align = get_sub_field( 'text_align' );
 $row_id = get_sub_field( 'row_id' );
 $cont_class = get_sub_field( 'cont_class' );
 $enable_curve = get_sub_field( 'enable_curve' );
 $curve_pos = get_sub_field( 'curve_pos' );


 $pattern_bg = get_sub_field( 'pattern_bg' );
 $pattern_type = get_sub_field( 'pattern_type' );
 $pattern_one_img = get_sub_field( 'pattern_one_img' );
 $pattern_two_img = get_sub_field( 'pattern_two_img' );
 $pattern_one_align = get_sub_field( 'pattern_one_align' );
 $pattern_two_align = get_sub_field( 'pattern_two_align' );
 gmaps_enqueue_scripts_styles();

?>


<section id="<?php echo $row_id ?>" class="layout-block frontpage-contact-block <?php echo $bg_colour ?> <?php echo $cont_padd ?> <?php echo $cont_class ?> <?php if ( $enable_curve == 'true' ) { ?><?php echo $curve_pos ?><?php } ?>" style="background: url('<?php echo $bg_image ?>')">


      <div class="contact-wrap <?php echo $cont_align ?>">

        <div class="rhs-map">
          <div id="map">

          </div>
        </div>

        <div class="rhs-contact">

          <div class="rhs-content">

          <?php if( have_rows('text') ): ?>
                <?php while( have_rows('text') ): the_row();
                $page_title = get_sub_field( 'page_title' );
                $title_color = get_sub_field( 'title_color' );
                    ?>
                    <a href="contact-us"><h2 class="page-title content-scroll <?php echo $title_color?>"><?php echo $page_title ?></h2></a>
              <?php endwhile; ?>
          <?php endif; ?>

          <div class="contact-icons">
              <?php if( have_rows('contact_icon') ): ?>
                  <?php while( have_rows('contact_icon') ): the_row();
                      $headline = get_sub_field( 'title' );
                      $desc = get_sub_field( 'description' );
                      $url = get_sub_field( 'url' );
                      $img = get_sub_field( 'icon' );

                      ?>

                        <a href="<?php echo $url ?>" target="_blank" class="contact-content content-scroll sequenced">
                          <div class="image">
                          <img class="svg" src="<?php echo $img ?>">
                          </div>
                          <div class="text">
                            <p class="title"><?php echo $headline ?></p>
                            <p class="desc"><?php echo $desc ?></p>
                          </div>
                        </a>

                  <?php endwhile; ?>
              <?php endif; ?>

          </div>


          </div>

        </div>

    </div>



    <?php if ( $pattern_bg == 'true' ) { ?>
        <?php if ( $pattern_type == 'one' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
        <?php } elseif ( $pattern_type == 'two' ) { ?>
          <div class="pattern-bg pattern-one" style="background:url('<?php echo $pattern_one_img ?>') <?php echo $pattern_one_align ?> no-repeat">
          </div>
          <div class="pattern-bg pattern-two" style="background:url('<?php echo $pattern_two_img ?>') <?php echo $pattern_two_align ?> no-repeat">
          </div>
        <?php } ?>
    <?php } ?>


        <?php if ( $enable_curve == 'true' ) { ?>
              <div class="layout-curve">
                <img class="svg" src="<?php echo get_template_directory_uri(); ?>/assets/images/patterns/rhs-rounded-corner-right.svg" />
              </div>
        <?php } ?>


</section>
