<?php
/**
 * Template part for displaying Front Page Hero layout block
 *
 */
?>


  <section class="rhs-hero front-page">

      <div id="front-hero-container">
        <div class="front-hero-slider">

        <?php if( have_rows('hero_slider') ): ?>
            <?php while( have_rows('hero_slider') ): the_row();
            $img_desktop = get_sub_field( 'hero_image_desktop' );
            $img_mobile = get_sub_field( 'hero_image_mobile' );
            $logo = get_sub_field( 'hero_logo' );
            $title = get_sub_field( 'hero_title' );
            $text = get_sub_field( 'hero_text' );
              slick_enqueue_scripts_styles();
          ?>
          <div class="front-hero-slide content-reveal">
            <div class="bg-image">
              <div class="bg-image-main bg-image-desktop" style="background:url('<?php echo $img_desktop ?>') no-repeat center center;" /></div>
              <div class="bg-image-main bg-image-mobile" style="background:url('<?php echo $img_mobile ?>') no-repeat center center;" /></div>
            </div>
            <div class="front-hero-content">
              <div class="front-hero-logo">
                <img class="svg" src="<?php echo $logo ?>">
              </div>
              <div class="front-hero-title">
                <h3><?php echo $title ?></h3>
              </div>
              <div class="front-hero-text">
                <p><?php echo $text ?></p>
              </div>
              <div class="front-hero-btn">
                <?php if( have_rows('button') ): ?>
                    <?php while( have_rows('button') ): the_row();
                    $enable_button = get_sub_field( 'enable_button' );
                    $button_type = get_sub_field( 'button_type' );
                    $button_text = get_sub_field( 'button_text' );
                    $button_colour = get_sub_field( 'button_color' );
                    $button_link = get_sub_field( 'button_link' );
                    $button_page = get_sub_field( 'button_page' );
                    $button_url = get_sub_field( 'button_url' );
                    $button_modal = get_sub_field( 'button_modal' );
                        ?>
                <?php if ( $enable_button == 'true' ) { ?>

                    <?php if ( $button_type == 'internal' ) { ?>

                        <a class="btn <?php echo $button_colour; ?>"  href=" <?php echo $button_link ?>">
                            <?php if ( !empty($button_text) )  { ?>
                                <?php echo $button_text ?>
                            <?php } ?>
                        </a>

                    <?php } elseif ( $button_type == 'page' ) { ?>

                        <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_page ?>">
                            <?php if ( $button_text )  { ?>
                                <?php echo $button_text ?>
                            <?php } ?>
                        </a>

                    <?php } elseif ( $button_type == 'external' ) { ?>

                        <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_url ?>" target="_blank">
                            <?php if ( $button_text )  { ?>
                                <?php echo $button_text ?>
                            <?php } ?>
                        </a>

                    <!-- Modal Button  -->
                    <?php } elseif ( $button_type == 'modal' ) { ?>

                        <!-- Contact Modal  -->
                        <?php if ( $button_modal == 'contact' ) { ?>

                        <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#register-modal">
                            <?php if ( $button_text )  { ?>
                                <?php echo $button_text ?>
                            <?php } ?>
                        </a>

                        <!-- Video Modal  -->
                        <?php } elseif ( $button_modal == 'video' ) { ?>

                        <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#video-modal">
                            <?php if ( $button_text )  { ?>
                                <?php echo $button_text ?>
                            <?php } ?>
                        </a>

                        <!-- Location Modal  -->
                        <?php } elseif ( $button_modal == 'location' ) { ?>

                            <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#location-modal">
                                <?php if ( $button_text )  { ?>
                                    <?php echo $button_text ?>
                                <?php } ?>
                            </a>

                        <?php } ?>


                    <?php } ?>

                <?php } ?>
                <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>

          </div>

          <?php endwhile; ?>
          <?php endif; ?>

        </div>

      </div>
    </section>
