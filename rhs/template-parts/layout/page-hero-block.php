<?php
/**
 * Template part for displaying  Page Hero layout block
 *
 */

 $cont_align = get_sub_field( 'text_align' );
 $vertical_align = get_sub_field( 'vertical_align' );
 $row_id = get_sub_field( 'row_id' );
 $cont_class = get_sub_field( 'cont_class' );
 $cont_width = get_sub_field( 'cont_width' );
 $bg_colour = get_sub_field( 'bg_color' );
 $img_desktop = get_sub_field( 'hero_image_desktop' );
 $img_mobile = get_sub_field( 'hero_image_mobile' );



 $logo = get_sub_field( 'hero_logo' );
 $enable_h1 = get_sub_field( 'enable_h1' );
 $title = get_sub_field( 'page_title' );
 $title_color = get_sub_field( 'title_color' );
 $subtitle = get_sub_field( 'subtitle' );
 $text = get_sub_field( 'hero_text' );
?>


<section id="<?php echo $row_id ?>" class="rhs-hero page-hero <?php echo $bg_colour ?>">

      <div class="hero-container">

        <div class="bg-image <?php echo $vertical_align ?>">
          <div class="bg-image-main bg-image-desktop" style="background:url('<?php echo $img_desktop ?>') no-repeat center center;" /></div>
          <div class="bg-image-main bg-image-mobile" style="background:url('<?php echo $img_mobile ?>') no-repeat center center;" /></div>
        </div>

        <div class="hero-content <?php echo $cont_align ?> <?php echo $vertical_align ?> content-scroll">
          <div class="wrap <?php echo $cont_width ?>">
              <div class="hero-logo">
                <img class="svg" src="<?php echo $logo ?>">
              </div>
              <div class="hero-title">
                <?php if ( $enable_h1 == 'true' ) { ?>
                    <h1 class="<?php echo $title_color?>"><?php echo $title ?></h1>
                <?php } else { ?>
                    <h2 class="page-title <?php echo $title_color?>"><?php echo $title ?></h2>
                <?php } ?>
              </div>
              <div class="hero-text">
                <p class="subtitle"><?php echo $subtitle ?></p>
                <p><?php echo $text ?></p>
              </div>
              <div class="hero-btn">
                <div class="buttons-wrap">
                  <?php if( have_rows('buttons') ): $list_count = 0; ?>
                      <?php while( have_rows('buttons') ): the_row();
                          $list_count++;
                          ?>
                          <?php if( have_rows('button') ): ?>
                                <?php while( have_rows('button') ): the_row();
                                $enable_button = get_sub_field( 'enable_button' );
                                $button_type = get_sub_field( 'button_type' );
                                $button_text = get_sub_field( 'button_text' );
                                $button_colour = get_sub_field( 'button_color2' );
                                $button_link = get_sub_field( 'button_link' );
                                $button_page = get_sub_field( 'button_page' );
                                $button_url = get_sub_field( 'button_url' );
                                $button_modal = get_sub_field( 'button_modal' );
                                $button_download = get_sub_field( 'button_download' );
                                    ?>
                            <?php if ( $enable_button == 'true' ) { ?>

                                <?php if ( $button_type == 'internal' ) { ?>

                                    <a class="btn <?php echo $button_colour; ?>"  href=" <?php echo $button_link ?>">
                                        <?php if ( !empty($button_text) )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>

                                <?php } elseif ( $button_type == 'page' ) { ?>

                                    <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_page ?>">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>

                                <?php } elseif ( $button_type == 'external' ) { ?>

                                    <a class="btn <?php echo $button_colour ?>"  href=" <?php echo $button_url ?>" target="_blank">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>

                                <?php } elseif ( $button_type == 'download' ) { ?>

                                    <a class="btn <?php echo $button_colour ?> download"  href=" <?php echo $button_download ?>" target="_blank">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                        <div class="download-icon"></div>
                                    </a>

                                <!-- Modal Button  -->
                                <?php } elseif ( $button_type == 'modal' ) { ?>

                                    <!-- Contact Modal  -->
                                    <?php if ( $button_modal == 'contact' ) { ?>

                                    <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#register-modal">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>

                                    <!-- Video Modal  -->
                                    <?php } elseif ( $button_modal == 'video' ) { ?>

                                    <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#video-modal">
                                        <?php if ( $button_text )  { ?>
                                            <?php echo $button_text ?>
                                        <?php } ?>
                                    </a>

                                    <!-- Location Modal  -->
                                    <?php } elseif ( $button_modal == 'location' ) { ?>

                                        <a class="btn <?php echo $button_colour ?>"  data-izimodal-open="#location-modal">
                                            <?php if ( $button_text )  { ?>
                                                <?php echo $button_text ?>
                                            <?php } ?>
                                        </a>

                                    <?php } ?>


                                <?php } ?>

                            <?php } ?>
                              <?php endwhile; ?>
                            <?php endif; ?>

                      <?php endwhile; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>


      </div>
    </section>
