<?php
/**
 * Template part for displaying page content in page.php
 */

?>



<?php if ( have_rows( 'content_layouts') ):
    while ( have_rows( 'content_layouts') ):the_row();

    $layout = get_row_layout();


    switch($layout) {
      case 'layout_page_hero':
          get_template_part('template-parts/layout/page-hero-block');
          break;
      case 'layout_front_intro':
          get_template_part('template-parts/layout/page-intro-block');
          break;
      case 'layout_text_block':
          get_template_part('template-parts/layout/page-text-block');
          break;
      case 'layout_text_image_block':
          get_template_part('template-parts/layout/page-text-image-block');
          break;
      case 'layout_table_block':
          get_template_part('template-parts/layout/page-table-block');
          break;
      case 'layout_image_block':
          get_template_part('template-parts/layout/page-image-block');
          break;
      case 'layout_icon_block':
          get_template_part('template-parts/layout/page-icon-block');
          break;
      case 'layout_text_button_block':
          get_template_part('template-parts/layout/page-text-button-block');
          break;
        case 'layout_quote_block':
            get_template_part('template-parts/layout/page-quote-block');
            break;
        case 'layout_accordion_block':
            get_template_part('template-parts/layout/page-accordion-block');
            break;
        case 'layout_contact_block':
            get_template_part('template-parts/layout/page-contact-form');
            break;
    }


    endwhile; endif; ?>
