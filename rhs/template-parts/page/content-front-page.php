<?php
/**
 * Template part for displaying page content in front-page.php
 *
 */

?>


<?php if ( have_rows( 'content_layouts') ):
    while ( have_rows( 'content_layouts') ):the_row();

    $layout = get_row_layout();

    switch($layout) {
        case 'layout_front_hero':
            get_template_part('template-parts/layout/frontpage-hero-block');
            break;
        case 'layout_front_intro':
            get_template_part('template-parts/layout/page-intro-block');
            break;
        case 'layout_front_icon':
            get_template_part('template-parts/layout/frontpage-icon-block');
            break;
        case 'layout_text_image_block':
            get_template_part('template-parts/layout/page-text-image-block');
            break;
          case 'layout_front_contact':
              get_template_part('template-parts/layout/frontpage-contact-block');
              break;
    }


    endwhile; endif; ?>
