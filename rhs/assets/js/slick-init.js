/* Slider Code */


(function($) {

    var $front_hero_slider = $('.front-hero-slider');
    var $icon_slider = $('.icon-slider');
    // var $contact_slider = $('.contact-slider');
    // var $ourschool_main_slider = $('.ourschool-mainslider');
    // var $ourschool_secondary_slider = $('.ourschool-secondaryslider');

    $( document ).ready(function() {
      $front_hero_slider.slick({
          arrows: false,
          fade: true,
          dots: true,
          rows: 0,
          speed: 750,
          autoplay: true,
          autoplaySpeed: 3500,
          infinite: true,
          swipe: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          swipeToSlide: true,
          touchThreshold: 10
      });
      $front_hero_slider.show();

        $icon_slider.slick({
            arrows: false,
            fade: false,
            rows: 0,
            speed: 750,
            autoplay: true,
            autoplaySpeed: 3500,
            infinite: true,
            swipe: true,
            swipeToSlide: true,
            touchThreshold: 10,
            responsive: [
                {
                    breakpoint: 9999,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        dots: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        dots: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 420,
                    settings: {
                        dots: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                }
            ]
        });
        $icon_slider.show();

        function checkslideWidth(){
          $vWidth = $(window).width();
          //Check condition for screen width
          if($vWidth > 1024){

                function loop() {
                    $(".icon-slider .slick-track").each(function() {
                        var current = $(this).children(".slick-current").removeClass("slick-current");
                        var i = current.next().length ? current.index() : 0;
                        current.siblings(":eq(" + i + ")").addClass("slick-current");
                    });
                }
                var interval = setInterval(loop, 3000);

                $(".icon-slide").hover(function() {
                  $(".icon-slide").removeClass('slick-current');
                  $(this).addClass('slick-current');
                   clearInterval(interval);
                  }, function() {
                        $(".icon-slide").removeClass('slick-current');
                        $(this).addClass('slick-current');
                        interval = setInterval(loop, 3000);
                  });

              }
          }
          checkslideWidth();


    });



})(jQuery);
